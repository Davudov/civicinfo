package com.example.android.politicalpreparedness.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.android.politicalpreparedness.network.models.Election

@Database(entities = [Election::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class ElectionDatabase: RoomDatabase() {
    abstract fun electionDao(): ElectionDao

    companion object {
        const val name = "Eelection_db"
        @Volatile
        private var INSTANCE: ElectionDatabase? = null

//        fun getInstance(context: Context): ElectionDatabase {
//            synchronized(this) {
//                var instance = INSTANCE
//                if (instance == null) {
//                    instance = Room.databaseBuilder(
//                            context.applicationContext,
//                            ElectionDatabase::class.java,
//                            "election_database"
//                    )
//                            .fallbackToDestructiveMigration()
//                            .build()
//
//                    INSTANCE = instance
//                }
//
//                return instance
//            }
//        }

    }

}