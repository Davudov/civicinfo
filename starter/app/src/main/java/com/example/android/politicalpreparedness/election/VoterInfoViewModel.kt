package com.example.android.politicalpreparedness.election

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.android.politicalpreparedness.database.ElectionDao
import com.example.android.politicalpreparedness.network.Result
import com.example.android.politicalpreparedness.network.models.Election
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class VoterInfoViewModel(private val dataSource: ElectionDao) : ViewModel() {
    var state = MutableLiveData(false)
    val opResult = MutableLiveData<Result<Boolean>>()


    fun getElectionInfo(argElectionId: Int) {
        viewModelScope.launch {
            withContext(Dispatchers.Main) {
                try {
                    val election = dataSource.getElectionById(argElectionId)
                    if (election !=null) {
                        Timber.d("${election.id}")
                        state.postValue(true)
                    }
                } catch (e: Exception) {
                    Timber.e(e)
                    state.postValue(false)
                }

            }
        }
    }

    fun changeFollowState(item: Election) {
        if (state.value == true) {
            deleteItem(item.id)
        } else {
            saveItem(item)
        }
    }

    private fun deleteItem(itemId: Int) {
        viewModelScope.launch {
            withContext(Dispatchers.Main) {
                try {
                    val result = dataSource.deleteItemById(itemId)
                    Timber.d("Item deleted row $result")
                    opResult.postValue(Result.Success(true))
                    state.postValue(false)

                } catch (e: Exception) {
                    Timber.e(e)
                    opResult.postValue(Result.Error(e))
                }
            }
        }
    }

    private fun saveItem(item: Election) {
        viewModelScope.launch {
            withContext(Dispatchers.Main) {
                try {
                    val result = dataSource.saveItem(item)
                    Timber.d("Item saved row $result")
                    opResult.postValue(Result.Success(true))
                    state.postValue(true)
                } catch (e: Exception) {
                    Timber.e(e)
                    opResult.postValue(Result.Error(e))
                }
            }
        }
    }

    //TODO: Add live data to hold voter info

    //TODO: Add var and methods to populate voter info

    //TODO: Add var and methods to support loading URLs

    //TODO: Add var and methods to save and remove elections to local database
    //TODO: cont'd -- Populate initial state of save button to reflect proper action based on election saved status

    /**
     * Hint: The saved state can be accomplished in multiple ways. It is directly related to how elections are saved/removed from the database.
     */

}

