package com.example.android.politicalpreparedness.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.android.politicalpreparedness.network.models.Election

@Dao
interface ElectionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveList(list: List<Election>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveItem(list: Election)

    @Query("delete from election_table where id =:argElectionId")
    suspend fun deleteItemById(argElectionId: Int)

    @Query("select * from election_table")
    suspend fun getElections(): List<Election>

    @Query("select * from election_table where id =:argElectionId")
    suspend fun getElectionById(argElectionId: Int): Election?
    //TODO: Add insert query

    //TODO: Add select all election query

    //TODO: Add select single election query

    //TODO: Add delete query

    //TODO: Add clear query

}