package com.example.android.politicalpreparedness.representative

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.android.politicalpreparedness.base.BaseViewModel
import com.example.android.politicalpreparedness.network.Result
import com.example.android.politicalpreparedness.network.models.Address
import com.example.android.politicalpreparedness.network.models.RepresentativeResponse
import com.example.android.politicalpreparedness.repo.RepresentativeRepo
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class RepresentativeViewModel(app: Application,
                              private val repo: RepresentativeRepo) : BaseViewModel(app) {

    val address = MutableLiveData("")
    val gpsValue = CompletableDeferred<LatLng>()

    val myLocationObservable = MutableLiveData<LatLng>()

    val data = MutableLiveData<Address>()

    val apiResultObserver = MutableLiveData<Result<RepresentativeResponse>>()

    //TODO: Establish live data for representatives and address

    //TODO: Create function to fetch representatives from API from a provided address

    /**
     *  The following code will prove helpful in constructing a representative from the API. This code combines the two nodes of the RepresentativeResponse into a single official :

    val (offices, officials) = getRepresentativesDeferred.await()
    _representatives.value = offices.flatMap { office -> office.getRepresentatives(officials) }

    Note: getRepresentatives in the above code represents the method used to fetch data from the API
    Note: _representatives in the above code represents the established mutable live data housing representatives

     */

    //TODO: Create function get address from geo location

    //TODO: Create function to get address from individual fields

    fun findMyLocation() {
        viewModelScope.launch {
            withContext(Dispatchers.Main) {
                try {
                    val myLocation = gpsValue.await()
                    myLocationObservable.postValue(myLocation)
                } catch (e: Exception) {
                    Timber.e(e)
                    e.printStackTrace()
                }
            }
        }
    }

    fun findRepresentatives() {
        val addressStr: String? = address.value
        addressStr ?: return
        showLoading.value = true
        viewModelScope.launch {
            withContext(Dispatchers.Main) {
                val result = repo.getRepresentatives(addressStr)
                apiResultObserver.postValue(result)
                showLoading.value = false
            }
        }
    }
}
