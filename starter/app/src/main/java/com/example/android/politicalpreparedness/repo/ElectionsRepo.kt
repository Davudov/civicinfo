package com.example.android.politicalpreparedness.repo

import com.example.android.politicalpreparedness.database.ElectionDao
import com.example.android.politicalpreparedness.network.CivicsApiService
import com.example.android.politicalpreparedness.network.Result
import com.example.android.politicalpreparedness.network.models.Election
import timber.log.Timber

class ElectionsRepo(private val electionDao: ElectionDao, private val civicsApiService: CivicsApiService) {

    suspend fun getElections(): Result<List<Election>> {
        return try {
            val result = civicsApiService.getElections().await()
            Result.Success(result.elections)
        } catch (e: Exception) {
            Timber.e(e)
            Result.Error(e)
        }
    }

    suspend fun getLocalElections(): Result<List<Election>> {
        return try {
            val result = electionDao.getElections()
            Result.Success(result)
        } catch (e: Exception) {
            Timber.e(e)
            Result.Error(e)
        }
    }
}