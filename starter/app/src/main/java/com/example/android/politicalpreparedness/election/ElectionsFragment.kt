package com.example.android.politicalpreparedness.election

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.android.politicalpreparedness.databinding.FragmentElectionBinding
import com.example.android.politicalpreparedness.election.adapter.ElectionListAdapter
import com.example.android.politicalpreparedness.network.Result
import com.example.android.politicalpreparedness.utils.setup
import org.koin.androidx.viewmodel.ext.android.viewModel

class ElectionsFragment : Fragment() {

    private lateinit var binding: FragmentElectionBinding
    private lateinit var navController:NavController
    private val viewModel: ElectionsViewModel by viewModel()
    private lateinit var upcomingAdapter: ElectionListAdapter
    private lateinit var localAdapter: ElectionListAdapter
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        navController = findNavController()


        binding = FragmentElectionBinding.inflate(inflater)

        localAdapter = ElectionListAdapter {
            navController.navigate(ElectionsFragmentDirections.actionElectionsFragmentToVoterInfoFragment(it))
        }
        upcomingAdapter = ElectionListAdapter {
            navController.navigate(ElectionsFragmentDirections.actionElectionsFragmentToVoterInfoFragment(it))
        }

        binding.savedRecyclerView.setup(localAdapter)
        binding.upcomingRecyclerView.setup(upcomingAdapter)

        viewModel.elections.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            upcomingAdapter.clear()
            when (it) {
                is Result.Success -> upcomingAdapter.addData(it.data)
                is Result.Error -> Toast.makeText(requireContext(), it.exception.message, Toast.LENGTH_SHORT).show()
            }
        })


        viewModel.localElections.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            localAdapter.clear()
            when (it) {
                is Result.Success -> localAdapter.addData(it.data)
                is Result.Error -> Toast.makeText(requireContext(), it.exception.message, Toast.LENGTH_SHORT).show()
            }
        })


        return binding.root
    }

    override fun onResume() {
        super.onResume()
        viewModel.getElections()
        viewModel.getLocalElections()
    }

}