package com.example.android.politicalpreparedness.election.adapter

import com.example.android.politicalpreparedness.R
import com.example.android.politicalpreparedness.base.BaseRecyclerViewAdapter
import com.example.android.politicalpreparedness.network.models.Election


//Use data binding to show the reminder on the item
class ElectionListAdapter(callBack: (selectElection: Election) -> Unit) :
        BaseRecyclerViewAdapter<Election>(callBack) {
    override fun getLayoutRes(viewType: Int) = R.layout.item_election_view

}
