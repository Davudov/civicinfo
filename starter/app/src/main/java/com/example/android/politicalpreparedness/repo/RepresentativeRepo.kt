package com.example.android.politicalpreparedness.repo

import com.example.android.politicalpreparedness.network.CivicsApiService
import com.example.android.politicalpreparedness.network.Result
import com.example.android.politicalpreparedness.network.models.RepresentativeResponse
import timber.log.Timber

class RepresentativeRepo(private val apiService: CivicsApiService) {
    suspend fun getRepresentatives(address:String): Result<RepresentativeResponse> {
        return try {
            val result = apiService.getRepresentatives(address).await()
            Result.Success(result)
        } catch (e: Exception) {
            Timber.e(e)
            Result.Error(e)
        }
    }
}