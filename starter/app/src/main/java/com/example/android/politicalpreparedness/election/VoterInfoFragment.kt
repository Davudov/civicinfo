package com.example.android.politicalpreparedness.election

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.example.android.politicalpreparedness.databinding.FragmentVoterInfoBinding
import com.example.android.politicalpreparedness.utils.BindingAdapters.setFollowText
import org.koin.androidx.viewmodel.ext.android.viewModel

class VoterInfoFragment : Fragment() {

    private lateinit var binding: FragmentVoterInfoBinding

    private val viewModel: VoterInfoViewModel by viewModel()
    private val args by navArgs<VoterInfoFragmentArgs>()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        binding = FragmentVoterInfoBinding.inflate(inflater)

        binding.data = args.item

        binding.viewModel = viewModel

        viewModel.state.observe(viewLifecycleOwner, Observer {
            binding.followBtn.setFollowText(it)
        })

        binding.stateLocations.setOnClickListener {
            goToUrl()
        }
        binding.stateBallot.setOnClickListener {
            goToUrl()
        }


        //TODO: Handle loading of URLs

        //TODO: Handle save button UI state
        //TODO: cont'd Handle save button clicks
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getElectionInfo(args.item.id)
    }

    //TODO: Create method to load URL intents

    private fun goToUrl() {
        val url = "https://myvote.wi.gov/en-us/"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }
}