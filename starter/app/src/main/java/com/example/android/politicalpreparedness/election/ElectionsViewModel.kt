package com.example.android.politicalpreparedness.election

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.android.politicalpreparedness.network.Result
import com.example.android.politicalpreparedness.network.models.Election
import com.example.android.politicalpreparedness.repo.ElectionsRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ElectionsViewModel(private val electionsRepo: ElectionsRepo) : ViewModel() {
    private val _elections = MutableLiveData<Result<List<Election>>>()
    val elections: LiveData<Result<List<Election>>> = _elections
    private val _localElections = MutableLiveData<Result<List<Election>>>()
    val localElections: LiveData<Result<List<Election>>> = _localElections
    val progress = MutableLiveData(false)


    fun getElections() {
        progress.postValue(true)
        viewModelScope.launch {
            withContext(Dispatchers.Main) {
                val result = electionsRepo.getElections()
                _elections.postValue(result)
                progress.postValue(false)
            }
        }
    }

    fun getLocalElections() {
        viewModelScope.launch {
            withContext(Dispatchers.Main) {
                val result = electionsRepo.getLocalElections()
                _localElections.postValue(result)
            }
        }
    }


}