package com.example.android.politicalpreparedness.network.models

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import java.io.Serializable
import java.util.*

@Entity(tableName = "election_table")
data class Election(
        @PrimaryKey val id: Int,
        @field: Json(name = "name")
        @ColumnInfo(name = "name")val name: String,
        @field: Json(name = "electionDay")
        @ColumnInfo(name = "electionDay") val electionDay: Date,
        @Embedded(prefix = "division_") @field: Json(name = "ocdDivisionId") val ocdDivisionId: Division
):Serializable