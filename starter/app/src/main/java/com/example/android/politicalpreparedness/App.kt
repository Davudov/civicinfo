package com.example.android.politicalpreparedness

import android.app.Application
import androidx.room.Room
import com.example.android.politicalpreparedness.database.ElectionDao
import com.example.android.politicalpreparedness.database.ElectionDatabase
import com.example.android.politicalpreparedness.election.ElectionsViewModel
import com.example.android.politicalpreparedness.election.VoterInfoViewModel
import com.example.android.politicalpreparedness.network.CivicsApi
import com.example.android.politicalpreparedness.repo.ElectionsRepo
import com.example.android.politicalpreparedness.repo.RepresentativeRepo
import com.example.android.politicalpreparedness.representative.RepresentativeViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import timber.log.Timber

class App : Application() {


    companion object {
        lateinit var INSTANCE: App
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        initKoin()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initKoin() {
        val moduleDefinition = module {

            viewModel {
                VoterInfoViewModel(get())
            }
            viewModel {
                RepresentativeViewModel(get(),get())
            }
            viewModel {
                ElectionsViewModel(get())
            }
            single {
                CivicsApi.retrofitService
            }
            single {
                ElectionsRepo(get(), get())
            }
            single {
                RepresentativeRepo(get())
            }

//            single {
//                MainRepo(get(), get())
//            }

            single { createDatabase() }

            single {
                createElectionDao(get())
            }

            single {  }
        }
        startKoin {
            androidContext(this@App)
            modules(listOf(moduleDefinition))
        }
    }


    private fun createDatabase(): ElectionDatabase {
        return Room.databaseBuilder(
                this@App,
                ElectionDatabase::class.java,
                ElectionDatabase.name
        )
                .fallbackToDestructiveMigration() //File to re-create the database, otherwise this will delete all of the data in the
//                * database tables managed by Room.
                .build()
    }

    private fun createElectionDao(appDatabase: ElectionDatabase): ElectionDao {
        return appDatabase.electionDao()
    }
}