package com.example.android.politicalpreparedness.representative

import android.Manifest
import android.content.Context
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Geocoder
import android.os.Bundle
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.example.android.politicalpreparedness.R
import com.example.android.politicalpreparedness.databinding.FragmentRepresentativeBinding
import com.example.android.politicalpreparedness.network.Result
import com.example.android.politicalpreparedness.network.models.Address
import com.example.android.politicalpreparedness.representative.adapter.RepresentativeListAdapter
import com.example.android.politicalpreparedness.representative.model.Representative
import com.example.android.politicalpreparedness.utils.LocationSettingsHelper
import com.example.android.politicalpreparedness.utils.LocationSettingsHelper.Companion.REQUEST_SETTINGS
import com.example.android.politicalpreparedness.utils.SettingsListener
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.fragment_representative.*
import kotlinx.android.synthetic.main.fragment_voter_info.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.util.*

class RepresentativeFragment : Fragment(), SettingsListener {
    private lateinit var binding: FragmentRepresentativeBinding
    private lateinit var mFusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationSettingsHelper: LocationSettingsHelper
    private val requestCode = 101
    private val viewModel: RepresentativeViewModel by viewModel()
    private lateinit var adapter: RepresentativeListAdapter

    companion object {
        //TODO: Add Constant for Location request
    }

    //TODO: Declare ViewModel

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentRepresentativeBinding.inflate(inflater)
        binding.viewModel = viewModel
        setLocationDependencies()
        locationSettingsHelper = LocationSettingsHelper(requireContext(), requireActivity(), this)

        adapter = RepresentativeListAdapter {

        }

        binding.representativeRecycler.adapter = adapter

        binding.buttonSearch.setOnClickListener {
            if (viewModel.address.value?.isEmpty() == true) {
                Toast.makeText(requireContext(), "Please add address", Toast.LENGTH_LONG).show()
            } else
                viewModel.findRepresentatives()
        }


        val arrayAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_dropdown_item, resources.getStringArray(R.array.states))
        binding.state.adapter = arrayAdapter

        viewModel.myLocationObservable.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            it ?: return@Observer
            val address = geoCodeLocation(LatLng(37.712342, -122.450353))
            binding.addressLine1.setText(address.line1)
            binding.addressLine2.setText(address.line2)
            binding.city.setText(address.city)
            binding.zip.setText(address.zip)
            val position = arrayAdapter.getPosition(address.state)
            binding.state.setSelection(position)
            try {
                hideKeyboard()
            } catch (e: Exception) {
                Timber.e(e)
            }
        })

        viewModel.apiResultObserver.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            when (it) {
                is Result.Success -> {
                    val representativeResponseList = it.data
                    val officeList = representativeResponseList.offices
                    val officialList = representativeResponseList.officials
                    val representativeList = officeList.zip(officialList) { office, official -> Representative(official, office) }
                    adapter.setData(representativeList)
                }
                is Result.Error -> Toast.makeText(requireContext(), it.exception.message, Toast.LENGTH_LONG).show()
            }
        })

        return binding.root
    }

    private fun setLocationDependencies() {
        val permissions = arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        )

        mFusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(requireContext())
        if (ActivityCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
        ) {
//            initMap()
        } else
            requestPermissions(permissions, requestCode)


        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                val receivedLocation = LatLng(
                        locationResult.lastLocation.latitude,
                        locationResult.lastLocation.longitude
                )

                Timber.v("Location Result: $locationResult")

                viewModel.gpsValue.complete(receivedLocation)

                Timber.d("Removing location updates")

                mFusedLocationProviderClient.removeLocationUpdates(this)
            }
        }

        /**start request for location*/
        startForLocationServices()
    }


    private fun createLocationRequest() =
            LocationRequest.create()?.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    ?.setInterval(50_000L)


    private fun startForLocationServices() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireContext())
        if (ActivityCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                    arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    ), requestCode
            )
            return
        }
        mFusedLocationProviderClient.requestLocationUpdates(
                createLocationRequest(),
                locationCallback, Looper.getMainLooper()
        )
    }

    private fun geoCodeLocation(location: LatLng): Address {

        val geocoder = Geocoder(context, Locale.getDefault())
        return geocoder.getFromLocation(location.latitude, location.longitude, 1)
                .map { address ->
                    Address(address.thoroughfare, address.subThoroughfare, address.locality, address.adminArea, address.postalCode)
                }
                .first()
    }

    private fun hideKeyboard() {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view!!.windowToken, 0)
    }

    override fun onResolvableApiException(e: ResolvableApiException) {
        // Location settings are not satisfied, but this can be fixed
        // by showing the user a dialog.
        // Location settings are not satisfied, but this can be fixed
        // by showing the user a dialog.
        try {
            // Show the dialog by calling startResolutionForResult(),
            // and check the result in onActivityResult().
            startIntentSenderForResult(e.resolution.intentSender, REQUEST_SETTINGS, null, 0, 0, 0, null)
        } catch (sendEx: IntentSender.SendIntentException) {
            // Ignore the error.
            Timber.e(sendEx)
        }
    }

    override fun onSettingsSatisfied() {

    }

    override fun onLocationAvailable(available: Boolean) {

    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == this.requestCode) {
            if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {

//                initMap()
                startForLocationServices()
            } else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                Toast.makeText(
                        requireContext(),
                        "Permission denied",
                        Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

}